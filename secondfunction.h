int kthsmallest(int arr[],int l,int r,int k)
{
    if(k>0 && k<=r-1+1)
    {
        int n=r-1+1;
        
        int i,median[(n+4)/5];
        
        for(i=0;i<n/5;i++)
            median[i] = find median(arr+l+i*5,5);
        if(i*5<n)
        {
            median[i]=find median(arr+l+i*5,n%5);
            i++;
        }
        
        int medofmed = (i==1)? median[i-1]:kthsmallest(median,0,i-1,i/2);
        
        int pos= partition(arr,l,r,medofmed);
        
        if(pos-1==k-1)
            return arr[pos];
        if(pos-1 >k-1)
            return kthsmallest(arr ,l, pos-1,k);
            
        return kth smallest (arr,pos+1,r,k-pos+l-1);
        
        
    }
    
    return INT_MAX;
}